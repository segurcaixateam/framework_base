package com.everis.demo.SCA_ICARO.jaxb_pojos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class Add {
    @XmlAttribute(name = "xmlns")
    private String xmlns;

    private int intA;
    private int intB;

    public Add() {
        xmlns = "http://tempuri.org/";
    }

    public int getIntA() {
        return intA;
    }

    public void setIntA(int intA) {
        this.intA = intA;
    }

    public int getIntB() {
        return intB;
    }

    public void setIntB(int intB) {
        this.intB = intB;
    }


    @Override
    public String toString() {
        return "Add{" +
                "xmlns='" + xmlns + '\'' +
                ", intA=" + intA +
                ", intB=" + intB +
                '}';
    }
}
