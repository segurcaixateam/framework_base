package com.everis.demo.SCA_ICARO.jaxb_pojos;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "soap:Envelope")
@XmlAccessorType(XmlAccessType.FIELD)
public class Envelope {
    @XmlAttribute(name="xmlns:xsi")
    private String xsi;
    @XmlAttribute(name="xmlns:xsd")
    private String xsd;
    @XmlAttribute(name="xmlns:soap")
    private String soap;

    public Envelope() {
        xsi = "http://www.w3.org/2001/XMLSchema-instance";
        xsd = "http://www.w3.org/2001/XMLSchema";
        soap = "http://schemas.xmlsoap.org/soap/envelope/";
    }


    @XmlElementWrapper(name = "soap:Body")
    @XmlElement(name = "Add")
    private List<Add> body;

    public List<Add> getBody() {
        return body;
    }

    public void setBody(List<Add> body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Envelope{" +
                "xsi='" + xsi + '\'' +
                ", xsd='" + xsd + '\'' +
                ", soap='" + soap + '\'' +
                ", body=" + body +
                '}';
    }
}
