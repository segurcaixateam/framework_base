package com.everis.demo.SCA_ICARO.jaxb_pojos;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import java.io.File;

public class XMLUpdate {

    public void addxmlupdate() throws JAXBException, XMLStreamException {

        //En esta linea se lee el objeto del archivo FicheroPrueba.xml
        Envelope envelope = unmarshalXml();
        System.out.println("Valor del objeto recien leido = " + envelope + "\n");

        //Aqui accedo al objeto add que esta dentro de envelope y hago una modificacion
        Add add = envelope.getBody().get(0);
        add.setIntA(1);
        add.setIntB(5);

        //Y lo "escribo". Realmente solo lo saca por pantalla hasta que se sustituya el System.out
        marshallXml(envelope);
    }

    private static Envelope unmarshalXml() throws JAXBException, XMLStreamException {
        String cwd = System.getProperty("user.dir");
        String xml_path = (cwd + "\\data_xml\\SoapRequestFile.xml");

        JAXBContext jc = JAXBContext.newInstance(Envelope.class);
        File xml = new File(xml_path);

        XMLInputFactory xif = XMLInputFactory.newFactory();
        xif.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, false);
        StreamSource source = new StreamSource(xml);
        XMLStreamReader xsr = xif.createXMLStreamReader(source);
        Unmarshaller unmarshaller = jc.createUnmarshaller();

        return (Envelope) unmarshaller.unmarshal(xsr);
    }

    private static void marshallXml(Envelope envelope) throws JAXBException {
        String cwd = System.getProperty("user.dir");
        String xml_path = (cwd + "\\data_xml\\SoapRequestFile.xml");
        File xml = new File(xml_path);
        JAXBContext jc = JAXBContext.newInstance(Envelope.class);

        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(envelope, xml);
    }


}
