package com.everis.frameworktochange.functionaltest.steps;

import com.everis.frameworktochange.functionaltest.utils.ActionsUtils;
import com.everis.frameworktochange.functionaltest.utils.BrowserDriver;
import com.everis.frameworktochange.functionaltest.utils.WaitUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.Alert;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class HookSteps {

    @Autowired
    private BrowserDriver browserDriver;

    @Autowired
    private ActionsUtils actionsUtils;

    @Before
    public void openBrowser() {

        try {
            browserDriver.getCurrentDriver();
            actionsUtils.browserConfiguration();
        } catch (RuntimeException ignore) {
            ignore.getCause();
        }
    }

    @After
    public void embedScreenshot(Scenario scenario) {
        try {
            actionsUtils.takeScreenshot(scenario);
            System.out.println("Screenshot made");
        } catch (Exception ignore) {
            ignore.getCause();
        }
        WaitUtils waitUtils = new WaitUtils();
        if(scenario.isFailed()){
            try{
                Alert alert = waitUtils.waitUntilAlert(10);
                System.out.println("Alert no controlado: "+ alert.getText());
                alert.accept();
            }catch(Exception ignore){
                ignore.getCause();
            }
        }

        browserDriver.closeBrowser();

        try{
            Runtime.getRuntime().exec("taskkill /f /im IEDriverServer.exe");
            Runtime.getRuntime().exec("taskkill /f /im iexplore.exe");
            //process.destroy();
            System.out.println("Proceso IEDriverServer.exe matado");
        } catch (IOException ignore) {
            ignore.getCause();
        }
    }

}
