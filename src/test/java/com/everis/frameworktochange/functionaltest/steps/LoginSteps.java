package com.everis.frameworktochange.functionaltest.steps;

import com.everis.frameworktochange.functionaltest.pageobjects.login.LoginService;
import com.everis.frameworktochange.functionaltest.utils.ActionsUtils;
import com.everis.frameworktochange.functionaltest.utils.ElementsUtils;
import com.everis.frameworktochange.functionaltest.utils.WaitUtils;
import com.everis.frameworktochange.functionaltest.dtos.UserCredentialsDto;
import cucumber.api.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;

public class LoginSteps {

    @Autowired
    private LoginService loginService;

    @Autowired
    private ActionsUtils actionsUtils;

    @Autowired
    private WaitUtils waitUtils;


    @Given("^I am in the 'new simulation' page as Admin$")
    public void goToSimulationPage() throws InterruptedException {
        actionsUtils.navigateTo(ElementsUtils.Pages.NEW_SIMULATION_PAGE);
        loginService.doNewLogin(new UserCredentialsDto(ElementsUtils.Login.USERNAME1, ElementsUtils.Login.PASSWORD1));
        Thread.sleep(1000000);
    }

}
