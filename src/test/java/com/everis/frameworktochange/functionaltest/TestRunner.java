package com.everis.frameworktochange.functionaltest;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(

        strict = true,
        features = "src/test/resources/com/everis/frameworktochange/functionaltest/",
        format = {"json:target/cucumber-report/cucumber.json"},
        monochrome = false,
        tags = "~@ignore",
        plugin = {"pretty"}

)
public class TestRunner {
}
