package com.everis.frameworktochange.functionaltest.pageobjects.login;

import com.everis.frameworktochange.functionaltest.dtos.UserCredentialsDto;
import com.everis.frameworktochange.functionaltest.utils.BrowserDriver;
import com.everis.frameworktochange.functionaltest.utils.WaitUtils;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    private BrowserDriver browserDriver;
    private WaitUtils waitUtils;
    private LoginPage loginPage;

    @Autowired
    public LoginServiceImpl(BrowserDriver browserDriver, WaitUtils waitUtils) {
        this.browserDriver = browserDriver;
        this.waitUtils = waitUtils;
    }

    @Override
    public void doNewLogin(UserCredentialsDto userCredentialsDto) throws InterruptedException {
        initPageFactory();
        setUserName(userCredentialsDto.getUsername());
        setPwd(userCredentialsDto.getPassword());
        submitLogin();
    }


    private void initPageFactory() {
        loginPage = PageFactory.initElements(this.browserDriver.getCurrentDriver(), LoginPage.class);
    }


    private void setUserName(String userName) {
        waitUtils.waitUntilClickable(loginPage.getUsernameField(), 10);
        loginPage.getUsernameField().sendKeys(userName);
    }

    private void setPwd(String pwd) {
        loginPage.getPasswordField().sendKeys(pwd);
    }

    private void submitLogin() {
        waitUtils.clickJS(loginPage.getSubmitBtn());
        waitUtils.waitForPageToLoad();
    }

}
