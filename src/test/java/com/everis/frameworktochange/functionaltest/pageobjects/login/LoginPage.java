package com.everis.frameworktochange.functionaltest.pageobjects.login;

import com.everis.frameworktochange.functionaltest.pageobjects.common.CommonPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends CommonPage {

    @FindBy(id = "NombreUsuario")
    private WebElement usernameField;

    @FindBy(id = "Password")
    private WebElement passwordField;

    @FindBy(id = "Button1")
    private WebElement submitBtn;

    @FindBy(id="lblTextoError")
    private WebElement loginErrorText;

    public WebElement getLoginErrorText(){ return loginErrorText; }

    public WebElement getUsernameField() {
        return usernameField;
    }

    public WebElement getPasswordField() {
        return passwordField;
    }

    public WebElement getSubmitBtn() {
        return submitBtn;
    }
}
