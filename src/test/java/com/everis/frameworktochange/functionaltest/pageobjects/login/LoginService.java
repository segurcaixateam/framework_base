package com.everis.frameworktochange.functionaltest.pageobjects.login;

import com.everis.frameworktochange.functionaltest.dtos.UserCredentialsDto;

public interface LoginService {

    void doNewLogin(UserCredentialsDto userCredentialsDto) throws InterruptedException;

}
