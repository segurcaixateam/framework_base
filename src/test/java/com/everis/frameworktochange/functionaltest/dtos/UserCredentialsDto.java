package com.everis.frameworktochange.functionaltest.dtos;

import org.springframework.stereotype.Component;

@Component
public class UserCredentialsDto {

    private String username;
    private String password;

    public UserCredentialsDto() {
    }

    public UserCredentialsDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
