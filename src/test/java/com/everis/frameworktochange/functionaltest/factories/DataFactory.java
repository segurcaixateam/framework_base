package com.everis.frameworktochange.functionaltest.factories;

import org.apache.commons.lang3.RandomUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.apache.commons.lang3.RandomUtils.nextInt;

public class DataFactory {

    public static String generateStartDate(){
        Calendar date = new GregorianCalendar();
        String day = "01";
        String month ="0" + Integer.toString(date.get(Calendar.MONTH+2));
        String year = Integer.toString(date.get(Calendar.YEAR));

        return day+"/"+month+"/"+year;
    }

    public static String generateCIF(){
        String first = "A";
        String second = "28";
        String third = Integer.toString(nextInt(10000,99999));
        String chain = second + third;
        char[] characters = chain.toCharArray();
        char[] odd = new char[4];
        char[] even = new char[3];
        int j = 0;
        int k = 0;

        for(int i = 0;i<characters.length;i++){
            switch(i){
                case 0:
                case 2:
                case 4:
                case 6:
                    odd[j] = characters[i];
                    j++;
                    break;
                default:
                    even[k] = characters[i];
                    k++;
                    break;
            }
        }

        int evenAddition = 0;
        for(int i=0;i<even.length;i++){
            Integer num = Character.getNumericValue(even[i]);
            evenAddition = evenAddition + num;
        }

        int oddAddition = 0;
        for(int i = 0; i< odd.length; i++){
            int result = 0;
            Integer num = Character.getNumericValue(odd[i]);
            num = num * 2;
            int number = num;
            while(number > 0){
                result += number%10;
                number = number/10;
            }
            oddAddition = oddAddition+result;
        }

        int totalAddition = evenAddition + oddAddition;

        int unit = 0;
        while(totalAddition>0){
            unit+=totalAddition%10;
            totalAddition = 0;
        }

        String last = Integer.toString(10-unit);
        if(last=="10"){
            return first + chain + "0";
        }else {
            return first + chain + last;
        }
    }

    public static String getRandomPostalCode(){
        String mad = "2800";
        String rid = Integer.toString(nextInt(1,10));
        return mad + rid;
    }

    public static String getCCC(){
        Integer DC1 = 1;

        Random random = new Random();
        List<Integer> numbers = new ArrayList<Integer>();
        StringBuilder CCC = new StringBuilder();
        for(int i = 0;i<10;i++){
            numbers.add(random.nextInt(9)+1);
            CCC.append(numbers.get(i).toString());
        }

        Integer D = (numbers.get(0))
                +(numbers.get(1)*2)
                +(numbers.get(2)*4)
                +(numbers.get(3)*8)
                +(numbers.get(4)*5)
                +(numbers.get(5)*10)
                +(numbers.get(6)*9)
                +(numbers.get(7)*7)
                +(numbers.get(8)*3)
                +(numbers.get(9)*6);

        Integer DC2 = 11 - (D % 11);

        if(DC2 == 10){
            DC2 = 1;
        }
        if(DC2 == 11){
            DC2 = 0;
        }

        String DC = DC1.toString()+DC2.toString();

        String accountNumber = "00811461"+DC+CCC;

        return accountNumber;
    }

    public static String getCCCOld (){

        int DC1 = 1;

        Random random = new Random();

        int uno = random.nextInt(9)+1;
        int dos = random.nextInt(9)+1;
        int tres = random.nextInt(9)+1;
        int cuatro = random.nextInt(9)+1;
        int cinco = random.nextInt(9)+1;
        int seis = random.nextInt(9)+1;
        int siete = random.nextInt(9)+1;
        int ocho = random.nextInt(9)+1;
        int nueve = random.nextInt(9)+1;
        int diez = random.nextInt(9)+1;

        String CCC = Integer.toString(uno) + Integer.toString(dos) + Integer.toString(tres) + Integer.toString(cuatro) + Integer.toString(cinco) + Integer.toString(seis) + Integer.toString(siete) + Integer.toString(ocho) + Integer.toString(nueve) + Integer.toString(diez);

        int D = (uno*1)+(dos*2)+(tres*4)+(cuatro*8)+(cinco*5)+(seis*10)+(siete*9)+(ocho*7)+(nueve*3)+(diez*6);

        int DC2 = 11 - (D % 11);

        if(DC2 == 10){
            DC2 = 1;
        }
        if(DC2 == 11){
            DC2 = 0;
        }

        String DC = Integer.toString(DC1)+Integer.toString(DC2);

        String numCuenta = "00811461"+DC+CCC;

        return numCuenta;
    }

    public static boolean random() {
        return nextInt(0, 2) == 0;
    }

    public static String getNifRandom() {

        String[] wordDNI = {"T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"};

        Random randomGenerator = new Random();
        int max = 99999999;
        int min = 80000000;
        int numberNif = randomGenerator.nextInt(max - min) + min;
        String nif = numberNif + wordDNI[numberNif % 23];
        return nif;
    }

    public static String reduceDateToEntry(int dateToSet) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, dateToSet);
        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
        return format1.format(cal.getTime());

    }

    public static String getRandomString() {

        String[] letters = {"A", "E", "I", "O", "U", "Q", "W", "R", "T", "Y", "P", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "M"};
        String random = "";

        for (int i = 0; i < nextInt(5, 9); i++) {
            random = random + letters[nextInt(0, letters.length)];
        }
        return random;

    }

    public static String getRandomBirthDate() {

        return nextInt(1, 25) + "/" + nextInt(1, 12) + "/" + nextInt(1940, 1999);

    }

    public static String getRandomPhone() {

        return Integer.toString(nextInt(600000000, 699999999));

    }

    public static String getRandomEmail() {

        return getRandomString() + "@gmail.com";

    }

    public static String getRandomNumAddress() {

        return Integer.toString(nextInt(1, 99));

    }

    public static String getTodayDate() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);

    }
}
