package com.everis.frameworktochange.functionaltest.utils;



import static io.github.bonigarcia.wdm.DriverManagerType.IEXPLORER;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.ie.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.stereotype.Component;

@Component
public class BrowserDriver {





    private ChromeDriver mDriver;
//    private InternetExplorerDriver mDriver;


    public WebDriver getCurrentDriver() {
        if (mDriver == null) {
            initDriver();
        }
//        else if (mDriver instanceof InternetExplorerDriver && ((InternetExplorerDriver) mDriver).getSessionId() == null) {
//            initDriver();
//        }
        else if (mDriver instanceof ChromeDriver && ((ChromeDriver) mDriver).getSessionId() == null) {
            initDriver();
        }

        return mDriver;
    }

    public void initDriver() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        mDriver = new ChromeDriver(chromeOptions);
//
//        DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
//        capabilities.setCapability(("ignoreZoomSetting"), true);
//        capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
//        System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "/drivers/IEDriverServer.exe");
//        mDriver = new InternetExplorerDriver(capabilities);
    }

    public void closeBrowser() {
        try {
            getCurrentDriver().quit();
        } catch (Exception ignore) {
        }
    }
}
