package com.everis.frameworktochange.functionaltest.utils;


public class ElementsUtils {

    public static class Selenium {
        public static final String SELENIUM_HUB = "http://localhost:4444/wd/hub";
    }


    public static class Pages {
        public static final String NEW_SIMULATION_PAGE = "http://adpvvmp32/Suscripcion.Web/modulos_funcionales/Seguridad/LoginAdeslas.aspx";
    }

    public static class Login {
        public static final String USERNAME1 = "adpreextranet\\04584165N";
        public static final String PASSWORD1 = "Adeslas2020";
    }

    public static class titles{
    }


    public static class AssertMessages{
        public static final String SEARCHSIMULATIONERROR = "No se ha encontrado la simulación buscada";
        public static final String SIMULATIONNOTDELETED = "La simulación no se ha eliminado";
        public static final String NOPRINTPAGE = "No ha llegado a la ventana \"Imprimir\"";
        public static final String NOREQUESTPAGE = "No ha llegado a la ventana de Alta De Solicitud";
        public static final String NOPRINCIPALINREQUEST = "No se ha cargado el titular de la simulación en la solicitud";
        public static final String NORESULTS = "No se han cargado resultados";
        public static final String NOCHECKMYSIM = "El check 'Mis simulaciones' no está checkeado";
        public static final String NOFILEEXPORTED = "No se ha encontrado el fichero exportado";
        public static final String TARIFERROR = "La simulación tiene un error de tarificación";
        public static final String MAXSUPPLTRY = "Las pólizas utilizadas han dado errores de datos.";
        public static final String ADDMODULEERROR = "El asegurado no dispone de módulos para contratar";
        public static final String EGASERROR = "Error EGAS";
    }

}
