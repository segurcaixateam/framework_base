package com.everis.frameworktochange.functionaltest.utils;

import cucumber.api.Scenario;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import static com.everis.frameworktochange.functionaltest.utils.CommonUtils.convertSetToList;

@Service
public class ActionsUtils {

    private BrowserDriver browserDriver;
    private WaitUtils waitUtils;

    @Autowired
    public ActionsUtils(BrowserDriver browserDriver) {
        this.browserDriver = browserDriver;
    }

    public void browserConfiguration() {
        browserDriver.getCurrentDriver().manage().window().maximize();
    }

    public List<String> getHandleWindow() {
        return convertSetToList(browserDriver.getCurrentDriver().getWindowHandles());
    }

    public void switchWindow(List<String> windowList, int windowId) {
        browserDriver.getCurrentDriver().switchTo().window(windowList.get(windowId));
    }

    public void loadPage(String url) {
        browserDriver.getCurrentDriver().get(url);
    }

    public Set<Cookie> getCookies() {
        return browserDriver.getCurrentDriver().manage().getCookies();
    }

    public void deleteCookies() {
        browserDriver.getCurrentDriver().manage().deleteAllCookies();
    }

    public void deleteLocalStorage() {
        executeJavascriptCommand("window.localStorage.clear();");
    }

    public void executeJavascriptCommand(String command, WebElement element) {
        ((JavascriptExecutor) browserDriver.getCurrentDriver()).executeScript(command, element);
    }

    public Object executeJavascriptCommand(String command) {
        return ((JavascriptExecutor) browserDriver.getCurrentDriver()).executeScript(command);
    }

    public void navigateTo(String url) {
        System.out.println("Navigating to "+url);
        executeJavascriptCommand("this.document.location = " + "\'" + url + "'");
    }

    public String getItemFromLocalStorage(String item) {
        return (String) executeJavascriptCommand("return window.localStorage.getItem(" + "\'" + item + "');");
    }

    public void takeScreenshot(Scenario scenario) throws IOException {
        File scrFile = ((TakesScreenshot) browserDriver.getCurrentDriver()).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "/target/report/" + scenario.getName() + ".png"));
        attachScreenshotReport(scenario, scrFile);
    }

    private static void attachScreenshotReport(Scenario scenario, File scrFile) throws IOException {
        byte[] data = FileUtils.readFileToByteArray(scrFile);
        scenario.embed(data, "image/png");
    }


    public void switchFrameByElement(WebElement webElement) {
        browserDriver.getCurrentDriver().switchTo().frame(webElement);
    }

    public void switchFrameByIndex(int count) {
        browserDriver.getCurrentDriver().switchTo().frame(count);
    }

    public void switchToDefaultContent() {
        browserDriver.getCurrentDriver().switchTo().defaultContent();
    }
}
