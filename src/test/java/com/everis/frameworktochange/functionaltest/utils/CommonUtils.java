package com.everis.frameworktochange.functionaltest.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.apache.commons.lang3.RandomUtils.nextInt;

public class CommonUtils {

    public static List<String> convertSetToList(Set<String> setToConvert) {
        List<String> mainList = new ArrayList<>();
        mainList.addAll(setToConvert);
        return mainList;
    }
    public void writeResults(String cif, String fileName) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            File file = new File(System.getProperty("user.dir")+File.separator+"DataFiles"+File.separator+fileName+".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(cif);
            bw.newLine();
            System.out.println("información agregada!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                //Cierra instancias de FileWriter y BufferedWriter
                if (bw != null) bw.close();
                if (fw != null) fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public String readResult(String fileName) throws IOException {
        ArrayList<String> arrayDocs = new ArrayList<String>();
        BufferedReader br = new BufferedReader(new FileReader(System.getProperty("user.dir")+File.separator+"DataFiles"+File.separator+fileName+".txt"));
        String linea = br.readLine();
        while(linea!=null){
            arrayDocs.add(linea);
            linea = br.readLine();
        }

        int index = nextInt(0, arrayDocs.size());
        String doc = arrayDocs.get(index);
        reWrite(arrayDocs, index, fileName);
        return doc;
    }

    public void reWrite(ArrayList<String> arrayDocs, int index, String fileName) throws IOException {
        //VOLVEMOS A ESCRIBIR LOS DATOS EXCEPTO EL DEL ÍNDICE PASADO POR PARÁMETRO, PORQUE ES EL QUE HEMOS QUEMADO
        BufferedWriter bw2 = new BufferedWriter(new FileWriter(System.getProperty("user.dir")+File.separator+"DataFiles"+File.separator+fileName+".txt", false));
        for(int i = 0;i<arrayDocs.size();i++){
            if(i!=index){
                bw2.write(arrayDocs.get(i));
                bw2.newLine();
            }
        }
        bw2.close();
    }
}
