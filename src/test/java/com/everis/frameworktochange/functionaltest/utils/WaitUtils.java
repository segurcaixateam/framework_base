package com.everis.frameworktochange.functionaltest.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.openqa.selenium.support.ui.ExpectedConditions.not;

@Component
public class WaitUtils {

    @Autowired
    private BrowserDriver browserDriver;

    @Autowired
    private ActionsUtils actionsUtils;

    public WebDriverWait wait(int seconds) {
        return new WebDriverWait(browserDriver.getCurrentDriver(), seconds);
    }

    public void waitUntilVisible(WebElement element, int seconds) {
        wait(seconds).until(ExpectedConditions.visibilityOf(element));
    }

    public void fluentWaitUntilVisible(WebElement element, int seconds) {
        wait(seconds).ignoring(NoSuchElementException.class).until(ExpectedConditions.visibilityOf(element));
    }

    public void waitUntilClickable(WebElement element, int seconds) {
        wait(seconds).until(ExpectedConditions.elementToBeClickable(element));
    }

    public void fluentWaitForClickable(WebElement element, int seconds) {
        wait(seconds).ignoring(NoSuchElementException.class).until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitUntilInvisible(WebElement element, int seconds) {
        wait(seconds).until(not(ExpectedConditions.visibilityOf(element)));
    }

    public void waitUntilContainsAttribute(WebElement element, String attribute, String value, int seconds) {
        wait(seconds).until(ExpectedConditions.attributeContains(element, attribute, value));
    }

    public void waitUntilTextDisappear(String element, String value, int seconds) {
        wait(seconds).until(not(ExpectedConditions.textToBe(By.cssSelector(element), value)));
    }

    public void waitUntilTextAppear(String element, String value, int seconds) {
        wait(seconds).until(not(ExpectedConditions.textToBe(By.cssSelector(element), value)));
    }

    public Alert fluentWaitUntilAlert(int seconds){
        return wait(seconds).ignoring(NoAlertPresentException.class).until(ExpectedConditions.alertIsPresent());
    }

    public Alert waitUntilAlert(int seconds) {
        return wait(seconds).until(ExpectedConditions.alertIsPresent());
    }

    public void fluentWaitUntilUrlContains(String url, int seconds) throws InterruptedException {
        wait(seconds).ignoring(NoSuchElementException.class).until(ExpectedConditions.urlContains(url));
    }

    public void fluentWaitUntilUrlMatches(String regex, int seconds) {
        wait(seconds).ignoring(NoSuchElementException.class).until(ExpectedConditions.urlMatches(regex));
    }

    public void waitUntilWindowHandle(int windowId) throws InterruptedException {
        int i;
        for (i = 0; i <= 10; i++) {
            Thread.sleep(1000);
            if (actionsUtils.getHandleWindow().size() > 1) {
                actionsUtils.switchWindow(actionsUtils.getHandleWindow(), windowId);
                i = 10;
            }
        }
    }

    public void waitForPageToLoad() {
        try {
            Thread.sleep(200);
            int default_wait_time = 90;
            (new WebDriverWait(browserDriver.getCurrentDriver(), default_wait_time)).until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver d) {
                    return (((org.openqa.selenium.JavascriptExecutor) browserDriver.getCurrentDriver())
                            .executeScript("return document.readyState").toString().equals("complete"));

                }
            });
        } catch (Exception ex) {
        }
    }


    public void accessToMainWindow(){

        int numVentanas = browserDriver.getCurrentDriver().getWindowHandles().size();
        System.out.println("Numero de ventanas" +numVentanas);
        Set<String> windows = browserDriver.getCurrentDriver().getWindowHandles();
        browserDriver.getCurrentDriver().switchTo().window(windows.iterator().next());
    }

    public void clickJS(WebElement element){
        JavascriptExecutor executor = (JavascriptExecutor)browserDriver.getCurrentDriver();
        executor.executeScript("arguments[0].click();", element);
    }


}
