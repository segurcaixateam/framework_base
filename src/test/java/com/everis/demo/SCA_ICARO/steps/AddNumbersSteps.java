package com.everis.demo.SCA_ICARO.steps;

import com.everis.demo.SCA_ICARO.jaxb_pojos.XMLUpdate;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import io.restassured.RestAssured;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import org.apache.commons.io.IOUtils;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static io.restassured.RestAssured.given;

public class AddNumbersSteps {

   XMLUpdate xmlupdate = new XMLUpdate();

    @Given("I want to add numbers")
    public void iWantToMakeAnOperation() throws JAXBException, XMLStreamException {
    xmlupdate.addxmlupdate();
    }


     @When("I do add operation")
    public void idoaddoperation() throws IOException {
        String cwd = System.getProperty("user.dir");
        String xml_path = (cwd + "\\data_xml\\SoapRequestFile.xml");
        FileInputStream fileInputStream = new FileInputStream(
                new File(xml_path));

        RestAssured.baseURI = "http://www.dneonline.com/";

        Response response = given()
                .header("Content-Type", "text/xml; charset=utf-8")
                .header("Accept-Encoding", "gzip,deflate")
                .and()
                .body(IOUtils.toString(fileInputStream, "UTF-8"))
                .when()
                .post("/calculator.asmx".toString())
                .then()
                .statusCode(200)
                .and()
                .log().all()
                .extract().response();


        XmlPath jsXpath = new XmlPath(response.asString());//Converting string into xml path to assert
        String rate = jsXpath.getString("AddResult");
        System.out.println("Result returned is: " + rate);
    }

    @Then("you should see the result")
    public void youShouldSeeTheResult() {
        System.out.println("GOOD JOB!");
    }



}
